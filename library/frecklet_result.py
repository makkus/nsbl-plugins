#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: (c) 2018 Markus Binsteiner (@makkus) <makkus@frkl.io>

from __future__ import absolute_import, division, print_function

__metaclass__ = type

ANSIBLE_METADATA = {
    "metadata_version": "1.1",
    "status": ["preview"],
    "supported_by": "community",
}


DOCUMENTATION = """
---
author: "Markus Binsteiner (@makkus)"
module: frecklet_result
short_description: Register key/value pairs as results to freckles
description:
    - This module/action plugin is really only useful in combination with freckles (https://freckles.io)
options:
  keys:
    description:
      - a string or list of strings indicating the variable names to register
    required: true
  default:
    description:
      - the default value if the key is not present, or empty
    required: false
  fail_if_empty:
    description:
      - fail if the value for the variable is empty
    default: false
    type: bool
version_added: "2.6"


"""

EXAMPLES = """
# Example setting host facts using complex arguments
- frecklet_result:
     user: "{{ ansible_env.USER }}"

"""
