import copy
import os
import re

import yaml
from ansible.errors import AnsibleFilterError

class FilterModule(object):

    def filters(self):
        return {
            'very_basics_filter': self.box_very_basics_filter,
        }

    def box_very_basics_filter(self, result_string):

        try:
            result = yaml.safe_load(result_string["stdout"])
        except (Exception) as e:
            raise AnsibleFilterError('Error trying to parse box very basic facts: {}'.format(e))

        can_pwless_sudo = result.pop("can_passwordless_sudo")
        if can_pwless_sudo == 1:
            result["can_passwordless_sudo"] = False
        elif can_pwless_sudo == 0:
            result["can_passwordless_sudo"] = True
        else:
            raise AnsibleFilterError("Invalid value for 'can_passwordless_sudo' key: {}".format(can_pwless_sudo))

        git_xcode = result.pop("git_xcode")
        if git_xcode == 1:
            result["git_on_mac_available"] = True
        elif git_xcode == 0:
            result["git_on_mac_available"] = False
        else:
            raise AnsibleFilterError("Invalid value for 'git_xcode' key: {}".format(can_pwless_sudo))

        path = result.pop("path")
        result["path"] = path.split(":")

        freckle_files_dict = result.pop("freckle_files")
        dirs = {}
        for parent_path, f_files_dict in freckle_files_dict.items():
            freckle_files_invalid = {}
            freckle_files = {}
            for path, content in f_files_dict.items():
                if not content:
                    freckle_files[path] = {}
                    continue
                try:
                    c = yaml.safe_load(content)
                    freckle_files[path] = c
                except (Exception) as e:
                    freckle_files_invalid[path] = content

            dirs[parent_path] = {}
            dirs[parent_path]["freckle_files"] = freckle_files
            dirs[parent_path]["freckle_files_invalid"] = freckle_files_invalid
        result["freckle_files"] = dirs
        # executables = result.pop("executables")
        # temp = {}
        # for name, paths in executables.items():
        #     p = paths.split(":")
        #     p.remove("")
        #     temp[name] = list(set(p))
        # result["executables"] = temp

        # folders = result.pop("directories")
        # temp = {}
        # for name, paths in folders.items():
        #     p = paths.split("\n")

        return result
